# Kendrick Lamar - To Pimp A Butterfly
---
***Reasons the Album is the Best***
1. Inspirational lyrics
2. Powerful message
3. Fantastic production
---
## Notable Songs
- **Alright**
- **u**
- **Mortal Man**
---
>  "Loving you is complicated."
---
![TPAB Album Art](./tpab-album-cover.jpeg "To Pimp A Butterfly")

| **Track List** ||
|---------------|---| 
| **Track No.** | **Track Title** |
| 1.| **Wesley's Theory** | 
| 2.| **For Free? - Interlude** |
| 3.| **King Kunta** |
| 4.| **Institutionalized** | 
| 5.| **These Walls** |
| 6.| **u** |
| 7.| **Alright** |
| 8.| **For Sale? - Interlude** |
| 9.| **Momma** |
| 10.| **Hood Politics** |
| 11.| **How Much A Dollar Cost** |
| 12.| **Complexion (A Zulu Love)** |
| 13.| **The Blacker The Berry** |
| 14.| **You Ain't Gotta Lie (Momma Said)** |
| 15.| **i** |
| 16.| **Mortal Man** |
